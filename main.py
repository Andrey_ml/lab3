import torch
import torch.nn as nn
import torch.nn.functional as F
from sklearn.model_selection import train_test_split

import pandas as pd
import matplotlib.pyplot as plt


class Model(nn.Module):
  # Input layer (4 features of the flower) -->
  # Hidden Layer1 (number of neurons) -->
  # H2 (n) -->
  # output (3 classes of iris flowers)
  def __init__(self, in_features=4, h1=8, h2=9, out_features=3):
    super().__init__() # instantiate our nn.Module
    self.fc1 = nn.Linear(in_features, h1)
    self.fc2 = nn.Linear(h1, h2)
    self.out = nn.Linear(h2, out_features)

  def forward(self, x):
    x = F.relu(self.fc1(x))
    x = F.relu(self.fc2(x))
    x = self.out(x)

    return x

my_df = pd.read_csv('iris.csv')

# Change last column from strings to integers
my_df['variety'] = my_df['variety'].replace('Setosa', 0.0)
my_df['variety'] = my_df['variety'].replace('Versicolor', 1.0)
my_df['variety'] = my_df['variety'].replace('Virginica', 2.0)

# Train Test Split!  Set X, y
X = my_df.drop('variety', axis=1)
y = my_df['variety']

# Convert these to numpy arrays
X = X.values
y = y.values

# Convert X features to float tensors
X_test = torch.FloatTensor(X)

# Convert y labels to tensors 
y_test = torch.LongTensor(y)

new_model = Model()
new_model.load_state_dict(torch.load('iris_model.pt'))


# Evaluate Model on Test Data Set (validate model on test set)
with torch.no_grad():  # Basically turn off back propogation
  y_eval = new_model.forward(X_test) # X_test are features from our test set, y_eval will be predictions

lst = []

for i in range(len(y_eval)):
  lst.append(int(y_eval[i].argmax().item()))

pred_df = pd.DataFrame({'pred':lst})
pred_df.to_csv (r'predict.csv', index= False)